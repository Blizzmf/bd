#include "sort.h"


int direc(){
    char d;
    printf("Выберите направление сортировки :\n1-по возростанию\n2-по убыванию\n");
    do{
        d = getPressedButton();
    } while(d < '1' || d > '2');
    d = d - '0';
    return d;
}
void swap(List *l){
    ELEMENT *tmp = l->curr->next;
    l->curr->next = tmp->next;
    tmp->priv = l->curr->priv;
    if(l->curr->next != NULL)
        l->curr->next->priv = l->curr;
    if(tmp->priv != NULL)
        tmp->priv->next = tmp;
    tmp->next = l->curr;
    l->curr->priv = tmp;
    if(l->curr == l->head)
        l->head = tmp;
}
void GetCmp(List* l){
    Sort_menu choise = sortmenu();
    system("clear");
    int direction = direc();
    system("clear");
    switch(choise){
        case SORTBYNAME:
        {
            if(direction==1)
                BubbleSort(l,CmpByName,0);
            else
                BubbleSort(l,CmpByName,1);
            break;
        }
        case SORTBYUNN:
        {
            if(direction==1)
                BubbleSort(l,CmpByYnn,1);
            else
                BubbleSort(l,CmpByYnn,0);
            break;
        }
        case SORTBYFIO:
        {
            if(direction==1)
                BubbleSort(l,CmpByFio,0);
            else
                BubbleSort(l,CmpByFio,1);
            break;
        }
        case SORTBYDATE:
        {
            if(direction==1)
                BubbleSort(l,CmpByDate,1);
            else
                BubbleSort(l,CmpByDate,0);
            break;
        }
        case SORTBYEMAIL:
        {
            if(direction==1)
                BubbleSort(l,CmpByEmail,0);
            else
                BubbleSort(l,CmpByEmail,1);
            break;
        }
        case SORTBYREVIES:
        {
            if(direction==1)
                BubbleSort(l,CmpByRev,1);
            else
                BubbleSort(l,CmpByRev,0);
            break;
        }
        case SORTBYALLREV:
        {
            if(direction==1)
                BubbleSort(l,CmpByAllRev,1);
            else
                BubbleSort(l,CmpByAllRev,0);
            break;
        }

        case MEN:
        {
            menuin(l);
            break;
        }
    }
}
void BubbleSort(List* l, int (*compare)(Firm* f1, Firm* f2, int dir), int direct){
    ELEMENT *top = l->head;
	while(top){
		ELEMENT *top2 = l->head;
		while(top2){
			if(compare(top->value,top2->value,direct)>0){
				Firm *tmp;
				tmp = top->value;
				top->value = top2->value;
				top2->value = tmp;
			}
			top2 = top2->next;
		}
	  top = top->next;
	}
}

int CmpByName(Firm* f1, Firm* f2, int dir){
    if ((dir == 1 && (strcmp(f1->name, f2->name) > 0))
    || (dir == 0 && (strcmp(f1->name, f2->name) < 0)))
    {
        return 1;
    }
    return -1;
}
int CmpByYnn(Firm* f1, Firm* f2, int dir){
    if ((dir == 1 && f1->ynn < f2->ynn)
    || (dir == 0 && f1->ynn > f2->ynn))
    {
        return 1;
    }
    return -1;
}
int CmpByFio(Firm* f1, Firm* f2, int dir){
    if ((dir == 1 && (strcmp(f1->Fio, f2->Fio) > 0))
    || (dir == 0 && (strcmp(f1->Fio, f2->Fio) < 0)))
    {
        return 1;
    }
    return -1;
}
int CmpByDate(Firm* f1, Firm* f2, int dir){
    int result;
    if(dir==0)
    {
        if((result = (f1->date.year-f2->date.year))!= 0)
        {
            return result;
        }
        else if((result = (f1->date.mm-f2->date.mm))!= 0)
        {
            return result;
        }
        else
        {
            return (f1->date.day-f2->date.day);
        }
    }
    if(dir==1)
    {
        if((result = (f1->date.year-f2->date.year))< 0)
        {
            return result;
        }
        else if((result = (f1->date.mm-f2->date.mm))< 0)
        {
            return result;
        }
        else
        {
            return (f1->date.day-f2->date.day);
        }
    }
}
int CmpByEmail(Firm* f1, Firm* f2, int dir){
    if ((dir == 1 && (strcmp(f1->email, f2->email) > 0))
    || (dir == 0 && (strcmp(f1->email, f2->email) < 0)))
    {
        return 1;
    }
    return -1;
}
int CmpByRev(Firm* f1, Firm* f2, int dir){
    if ((dir == 1 && f1->reviews < f2->reviews)
    || (dir == 0 && f1->reviews > f2->reviews))
    {
        return 1;
    }
    return -1;
}
int CmpByAllRev(Firm* f1, Firm* f2, int dir){
    if ((dir == 1 && f1->allrev < f2->allrev)
    || (dir == 0 && f1->allrev > f2->allrev))
    {
        return 1;
    }
    return -1;
}
