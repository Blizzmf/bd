#ifndef LIST_H
#define LIST_H
#include <stdio.h>

typedef struct _ELEMENT{
    void* value;
    struct _ELEMENT *priv;
    struct _ELEMENT *next;
}ELEMENT;

typedef struct{
    ELEMENT *head;
    ELEMENT *curr;
    ELEMENT *tail;
    size_t size;

}List;

List* CreateList();
int Append(List* list, void *value);
int Insert(List* list, void *value);
void* Delete(List* list);
void DestroyList(List *list);

void* getValue(List* list);
int setFirst(List* list);
int setEnd(List* list);
int next(List* list);
int hasNext(List* list);
int priv(List* list);
int hasPriv(List* list);
void cleanList(List* list);
#endif // LIST_H

