#ifndef CHECK
#define CHECK

int checkInput(char* str, void* value);
int readData(char* msg, int (*check)(char*, void*), void* value);
int checkNum(char* str, void* value);
int checkName(char* str, void* value);
int checkUNN(char* str, void* value);
int checkFIO(char* str, void* value);
int checkDate(char* str, void* value);

#endif // CHECK
