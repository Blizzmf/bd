#include "menu.h"
#define linux
#ifdef linux
#include <termios.h>

#endif
Menu main_menu(){
    system("clear");
    //printf("\033[0;0H");
	printf("________________________________________________________\n");
	printf("           \n");
	printf("                   \n");
	printf("________________________________________________________\n");
	printf("               1 - Вывести на экран                     \n");
	printf("               2 - Изменить запись                      \n");
	printf("               3 - Добавить запись                      \n");
	printf("               4 - Удалить запись                       \n");
	printf("               5 - Сортировка                           \n");
	printf("               6 - Поиск                                \n");
	printf("               7 - Прочитать с файла                    \n");
	printf("               8 - Записать в файл                      \n");
	printf("               9 - Очистить лист                        \n");
	printf("               0 - Выход                                \n");
	printf("________________________________________________________\n");
	printf("\n");
	char input; //= '0';
	do{
	input = getPressedButton();//getchar();
	}while (input < '0' || input > '9');
	Menu choice;
	choice = input - '0';
	return choice;
}

void menuin(List* firms){
	while(1)
	{
		Menu choise = main_menu();
		system("clear");
		switch(choise)
		{
			case PRINT:
			{
                ui();
                printf("\n");
                printlist(firms);
                system("sleep 300");
				break;
			}
			case CHANGE:
			{
                change(firms);
				break;
			}
			case EXIT:
			{
                DestroyList(firms);
                exit(0);
				break;
			}
			case WRITEFILE:
			{
                writelist(firms);
                //system("sleep 300");
				break;
			}
			case DEL:
			{
                DelFirm(firms);
                break;
            }
            case ADD:
            {
               add(firms);
               break;
            }
            case READFILE:
            {
                refreshID();
                readlist(firms);
               // system("sleep 300");
                break;
            }
            case SEARCH:
            {
                List* l2 = CreateList();
                searchByName(firms,l2);
                printlist(l2);
                system("sleep 300");
				break;
            }
            case CLEANLIST:
            {
                refreshID();
                cleanList(firms);
                break;
            }
            case SORTIT:
            {
                GetCmp(firms);
                ui();
                printlist(firms);
                system("sleep 300");
                break;

            }
			default:
				break;
		}
	}
	printf("12\n");
   // system("pause");
}
Change chmenu(){
    system("clear");
    //printf("\033[0;0H");
	printf("________________________________________________________\n");
	printf("          Выберите поле которое хотите изменить \n");
	printf("                   \n");
	printf("________________________________________________________\n");
	printf("               1 -  Название                            \n");
	printf("               2 -  УНН                                 \n");
	printf("               3 -  ФИО                                 \n");
	printf("               4 -  Дата основания                      \n");
	printf("               5 -  E-mail                              \n");
	printf("               6 -  Кол-во отзывов                      \n");
	printf("               7 -  Положительные отзывы                \n");
	printf("               0 -  В главное меню                      \n");
	printf("________________________________________________________\n");
	printf("\n");
	char input; //= '0';
	do{
	input = getPressedButton();//getchar();
	}while (input < '0' || input > '9');
	Menu choice;
	choice = input - '0';
	return choice;
}
Sort_menu sortmenu(){
    system("clear");
    //printf("\033[0;0H");
	printf("________________________________________________________\n");
	printf("          Выберите поле по которому хотите сортировать \n");
	printf("                   \n");
	printf("________________________________________________________\n");
	printf("               1 -  Название                            \n");
	printf("               2 -  УНН                                 \n");
	printf("               3 -  ФИО                                 \n");
	printf("               4 -  Дата основания                      \n");
	printf("               5 -  E-mail                              \n");
	printf("               6 -  Кол-во отзывов                      \n");
	printf("               7 -  Положительные отзывы                \n");
	printf("               0 -  В главное меню                      \n");
	printf("________________________________________________________\n");
	printf("\n");
	char input; //= '0';
	do{
	input = getPressedButton();//getchar();
	}while (input < '0' || input > '9');
	Menu choice;
	choice = input - '0';
	return choice;
}
#ifdef linux
int getPressedButton(){
	char c;
	struct termios tty, savetty;
	if (!isatty(0)) // Если stdin - не терминал, то работать смысла нет
	{
		printf("stdin is not a tty!\n");
		return -1;
	}
	fflush(stdout); // вывели буфер
	tcgetattr(0, &tty); // получили структуру termios
	savetty = tty; // сохранили
	tty.c_lflag &= ~(ISIG | ICANON|ECHO);
	// ISIG - when any of the characters  INTR,  QUIT,  SUSP,  or DSUSP are received, generate the corresponding signal.
	// ICANON - enable canonical mode.  This  enables  the  special characters  EOF,  EOL,  EOL2, ERASE, KILL, REPRINT, STATUS, and WERASE, and buffers by lines.
	tty.c_cc[VMIN] = 1;
	tcsetattr(0, TCSAFLUSH, &tty);
	read(0, &c, 1);
	tcsetattr(0, TCSANOW, &savetty);
	return c;

}
#else
int getPressedButton(){
	return getchar();
}
#endif
