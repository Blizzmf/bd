#include "input.h"
#include "check.h"


Firm* Input(){
    Firm * firm;
    firm = (Firm*)malloc(sizeof(Firm));
    #ifdef Input_all
    if(readData("Введите данны о фирме('Название' [УНН], ФИО, Дата основание)",checkInput,firm)){
        free(firm);
        return NULL;
    }
    #else
    if(readData("Введите название фирмы : ", checkName, firm->name)){
        //free(firm->name);
        return NULL;
    }
    readData("Введите UNN : ", checkUNN, &firm->ynn);
    readData("Введите ФИО : ", checkFIO, firm->Fio);
    readData("Введите дату основания фирмы : ", checkDate, &firm->date);
    readData("Введите кол-во отзывов : ", checkNum, &firm->allrev);
    readData("Введите кол-во положтельных отзывов в %(мин 0,макс 100) : ", checkNum, &firm->reviews);
    readData("Введите e-mail фирмы : ", checkName, &firm->email);
    #endif // Input_all
    return firm;
}
int readData(char* msg, int (*check)(char*, void*), void* value){
    char* str = (char*) calloc(128, sizeof(char));
    int res = 0;
    if(InputStreem==stdin)
        printf("%s\n", msg);
//    fflush(stdin);
    __fpurge(stdin);
    while(1){
        //gets(str);
        fgets(str, 128, InputStreem);
        if(strcmp(str,"\n")==0){
            res = 1;
            break;
        }
        if(check(str, value)){
            if(InputStreem==stdin)
                printf("Ошибка ввода, повторите ввод\n");
        }else{
            break;
        }
    }
    free(str);
    return res;
}

void printFirm(Firm * firm){
    printf("%d |\"%s\"  | %10ld | %5s |    %02d-%02d-%04d    |   %5s |%5d %5d       |\n",firm->id,firm->name,firm->ynn,firm->Fio,firm->date.day,firm->date.mm,firm->date.year,firm->email,firm->reviews,firm->allrev);
    //printf("+--+--------+---------+------+----------------------+-------+--------------+\n");
}
void ui(){

    printf("№ |Название|        УНН    |     ФИО    |   Дата основания |      E-mail        | полож. отзывов из|");
    //printf("+------+-------------------------+-------------------------+------+--------+-----------+----------+\n");
}
int getFreeID(){
    //LAST_ID++;
    return LAST_ID++;
}
void refreshID(){
    LAST_ID=1;
}
void add(List* l){
    Firm* firm = NULL;
 //   while(1){
        firm = Input();
 //       if(firm==NULL)
//            break;
        firm->id = getFreeID();
        Append(l, firm);
  //  }
}

void writelist(List* l){
    Firm* firm = NULL;
    int i;
    FILE* OutputStreem= fopen("Firms","wb");
    fwrite(&l->size,sizeof(l->size),1,OutputStreem);
    for(i = setFirst(l); i; i = next(l)){
        firm = getValue(l);

        fwrite(firm,sizeof(Firm),1,OutputStreem);
    }
    fclose(OutputStreem);
}
void readlist(List* l){
    cleanList(l);
    Firm* firm = NULL;
    FILE* FileStreem= fopen("Firms","rb");
    int i,num=1;
    size_t n;
    fread(&n,sizeof(n),1,FileStreem);
    for(i = 0; i < n; i++){
        firm = (Firm*) malloc(sizeof(Firm));
        fread(firm,sizeof(Firm),1,FileStreem);
        firm->id = getFreeID();
        Append(l, firm);
    }
    fclose(FileStreem);
}

void printlist(List* l){
    Firm* firm = NULL;
    int i;
    for(i = setFirst(l); i; i = next(l)){
        firm = getValue(l);
        printFirm(firm);
    }
}
void strToLower(char *c1, char* c2){
	int len = strlen(c1);
	int i;
	for(i = 0; i<= len; i++){
		c2[i] = tolower(c1[i]);
	}
}
int searchByName(List* l, List*l2){
    int i, j=0;
    char *val;
    Firm* firm = NULL;
    val=(char*)calloc(20,sizeof(char));
    readData("Введите строку для поиска : ", checkName, val);
    strToLower(val, val);
    char str[20];
    for(i = setFirst(l); i ; i=next(l)){
        firm = getValue(l);

        strToLower(firm->name, str);
        if((strstr(str, val)!=NULL)){//search
        //if((strstr(str, val)==NULL)){//delite
            Append(l2, firm);
        }
    }
 //   *ff2 = f2;
    //free(str);
    return 0;
}
void printfsearch(Firm* s_firm,int s_size){
    int id;
    for(id=0;id<s_size;id++){
        printFirm(&s_firm[id]);
    }

}
void change(List* l){
    int ind = 0;
    Firm* firm = NULL;
    printlist(l);
    printf("\n\n\n");
    printf("Введите индекс элемента который хотите изменить : ");
    scanf("%d",&ind);
    int p = setFirst(l);
    for(int i=0 ;i < ind-1; i++){
        p = next(l);
    }
    firm = getValue(l);
    while(1){
        Change choise = chmenu();
        system("clear");
        switch(choise){
            case NAME:
            {
               // free(firm->name);
                readData("Введите название фирмы : ", checkName, firm->name);
                break;
            }
            case YNN:
            {
                //free(firm->ynn);
                readData("Введите UNN : ", checkUNN, &firm->ynn);
                break;
            }
            case FIO:
            {
               // free(firm->Fio);
                readData("Введите ФИО : ", checkFIO, firm->Fio);
                break;
            }
            case DATA:
            {
               readData("Введите дату основания фирмы : ", checkDate, &firm->date);
               break;
            }
            case ALLREV:
            {
               // free(firm->allrev);
                readData("Введите кол-во отзывов : ", checkNum, &firm->allrev);
                break;
            }
            case REV:
            {
               // free(firm->reviews);
                readData("Введите кол-во положтельных отзывов в %(мин 0,макс 100) : ", checkNum, &firm->reviews);
                break;
            }
            case EMAIL:
            {
               // free(firm->email);
                readData("Введите e-mail фирмы : ", checkName, &firm->email);
                break;
            }
            case MEN:
            {
                menuin(l);
                break;
            }

        }
    }
}
void DelFirm(List* l){
    int ind = 0;
    Firm* firm = NULL;
    printlist(l);
    printf("\n\n\n");
    printf("Введите индекс фирмы которую хотите удалить : ");
    scanf("%d",&ind);
    for(int i = setFirst(l) ; i ; i = next(l)){
        firm = getValue(l);
    }
    free(getValue(l));
    Delete(l);
}
