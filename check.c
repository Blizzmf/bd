#include "check.h"
#include "input.h"

int checkNum(char* str, void* value){
    if(sscanf(str,"%d", (int*)value)!=1){
        return -1;
    }
    return 0;
}
int checkName(char* str, void* value){
  //  *((char**)value) = (char*) calloc(strlen(str), sizeof(char));
    if(sscanf(str,"%s", ((char*)value))!=1){
        return -1;
    }

    return 0;
}
int checkUNN(char* str, void* value){
    if(sscanf(str,"%ld", (long*)value)!=1){
        return -1;
    }

    return 0;
}
int checkFIO(char* str, void* value){
  //  *((char**)value) = (char*) calloc(strlen(str), sizeof(char));
    char* e=str;

    char* dst=((char*) value);
    while(*e!='\0'){
        if(!(isalpha(*e)||isspace(*e)))
            return -1;
        e++;
    }
    char n[20], m[20];
    if(sscanf(str,"%s %s %s", dst, &n, &m)!=3){
      //  printf("%s %c %c", value, n, m);
        return -1;
    }
    sprintf(dst, "%s %s %s", dst, n, m);
    //printf("%s\n",dst);

    return 0;
}
int checkDate(char* str, void* value){
    Date* date = (Date*) value;
    if(sscanf(str,"%d %d %d", &date->day, &date->mm, &date->year)!=3){
        return -1;
    }
    if(date->day<1||date->day>31)
        return -1;
    if(date->mm<1||date->mm>12)
        return -1;

    return 0;
}
int checkInput(char* str, void* value){
	Firm* firm = (Firm*) value;
	char * e = str;
	e = strchr(e, '"');
	if(!e) return -1;
	e++;
	str = strchr(e, '"');
	if (!str) return -1;
	*str='\0';
	str++;
	if (checkName(e, &firm->name))
		return -1;
    if(sscanf(str," [%ld]",&firm->ynn)!=1)
		return -1;
	e = strchr(str, ',');
	if (!e)
		return -1;
	e++;
	str = strchr(e, ',');
	if (!str)
		return -1;
	*str = '\0';
	str++;
	if (checkFIO(e, firm->Fio))
		return -1;
	if (checkDate(str, &firm->date))
		return -1;
    return 0;
}
