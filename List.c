#include "List.h"

List* CreateList(){
    List* l = (List*) malloc(sizeof(List));
    l->curr = NULL;
    l->head = NULL;
    l->tail = NULL;
    l->size = 0;
    return l;

}
int Insert(List* list, void* value){
    ELEMENT *e = (ELEMENT*) malloc(sizeof(ELEMENT));
    if(e==NULL)
        return 0;
    e->value = value;
    e->priv = NULL;
    e->next = NULL;
   // if(list->head == NULL){
   if(list->size==0){
        list->head = e;
        list->curr = e;
        list->tail = e;
    }else{
        if(list->curr->next){
            list->curr->next->priv = e;
            e->next = list->curr->next;
        }else{
            list->tail = e;
        }
        list->curr->next = e;
        e->priv = list->curr;
    }
    list->size++;
    return 1;
}

int Append(List* list, void* value){
    list->curr = list->tail;
    return Insert(list, value);
}

void* Delete(List* list){
    if(!list->curr)
        return NULL;

    void* val = list->curr->value;
    ELEMENT* e = list->curr;

//    printf("\n%s\n", e->priv->value);
 //   printf("%s\n\n", e->next->value);
    if(e->priv){
        e->priv->next=e->next;
        list->curr = e->priv;
    }else
        list->head = e->next;

    if(e->next){
        e->next->priv=e->priv;
        list->curr = e->next;
    }else
        list->tail = e->priv;
  //  list->curr = (e->next)?e->next:e->priv;
    list->size--;
    free(e);
    return val;
}
int next(List* list){
    if(!hasNext(list))
        return 0;
    list->curr = list->curr->next;
    return 1;
}

int priv(List* list){
    if(!hasPriv(list))
        return 0;
    list->curr = list->curr->priv;
    return 1;
}
int hasNext(List* list){
    if(!list->curr || !list->curr->next)
        return 0;
    return 1;
}
int hasPriv(List* list){
    if(!list->curr || !list->curr->priv)
        return 0;
    return 1;
}
int setFirst(List* list){
    if(!list->head)
        return 0;
        list->curr = list->head;
    return 1;

}
int setEnd(List* list){
    if(!list->tail)
        return 0;
        list->curr = list->tail;
    return 1;

}
void* getValue(List* list){
    if(list->curr)
        return list->curr->value;
    return NULL;
}
void DestroyList(List* list){
    ELEMENT* e;
    if(list->size > 0){
        e = list->head;

        while(next(list)){
            free(e);
            e = list->curr;
        }
        free(e);
    }
    free(list);
}
void cleanList(List* list){
    int i=0;
    int size = list->size;
    setFirst(list);
    while(list->size){
        free(getValue(list));
        Delete(list);
        //list->size--;
    }
}
