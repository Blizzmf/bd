#ifndef MENU
#define MENU
#include <stdio.h>
#include "input.h"
typedef enum{
	PRINT = 1,
	CHANGE = 2,
    ADD = 3 ,
	DEL = 4,
	SORTIT = 5,
	SEARCH = 6,
	READFILE = 7,
	WRITEFILE = 8,
	CLEANLIST = 9,
	EXIT = 0
} Menu;

typedef enum{
	SORTBYNAME = 1,
	SORTBYUNN = 2,
	SORTBYFIO = 3,
	SORTBYDATE = 4,
	SORTBYEMAIL = 5,
	SORTBYREVIES = 6,
	SORTBYALLREV = 7,
	MAIN = 0,
}Sort_menu;

Menu main_menu();
Sort_menu sortmenu();
void menuin(List* firms);
int getPressedButton();

#endif
