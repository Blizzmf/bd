#ifndef SORT
#define SORT
#include "input.h"
#include "menu.h"


void BubbleSort(List* l, int (*compare)(Firm* f1, Firm* f2, int dir), int direct);
void swap(List* l);
void GetCmp(List* l);
int direc();
int CmpByName(Firm* f1, Firm* f2, int dir);
int CmpByYnn(Firm* f1, Firm* f2, int dir);
int CmpByFio(Firm* f1, Firm* f2, int dir);
int CmpByDate(Firm* f1, Firm* f2, int dir);
int CmpByEmail(Firm* f1, Firm* f2, int dir);
int CmpByRev(Firm* f1, Firm* f2, int dir);
int CmpByAllRev(Firm* f1, Firm* f2, int dir);
#endif // SORT

